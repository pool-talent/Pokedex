const POKEMONS = [
    {
        name: "CHARIZARD",
        type: "FIRE",
        number: 006,
        level: 100,
        hability: "FLAMES",
        height: "1.7 m",
        weight: "90.5 kg",
        typeImage: "Fire_Type_Icon.png",
        image: "Charizard.png"
    },
    {
        name: "SQUIRTLE",
        type: "WATER",
        number: 007,
        level: 50,
        hability: "TORRENT",
        height: "0.5 m",
        weight: "9.0 kg",
        typeImage: "Water.png",
        image: "squirtle.png"
    },
    {
        name: "MEWTWO",
        type: "PSYCHIC",
        number: 150,
        level: 100,
        hability: "PRESSURE",
        height: "2.0 m",
        weight: "122.0 kg",
        typeImage: "Psychic.png",
        image: "Mewtwo.png"
    },
    {
        name: "HAUNTER",
        type: "GHOST POSION",
        number: 093,
        level: 75,
        hability: "LEVITATE",
        height: "1.6 m",
        weight: "0.1 kg",
        typeImage: "Ghost.png",
        image: "haunter.png"
    },
    {
        name: "SNORLAX",
        type: "NORMAL",
        number: 143,
        level: 100,
        hability: "THICK FAT",
        height: "2.1 m",
        weight: "460.0 kg",
        typeImage: "Normal.png",
        image: "snorlax.png"
    }
];