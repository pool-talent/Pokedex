/**
 * Constantes
 */
const ROUTEIMAGES = "./images/";

/**
 * Función encargada de cambiar los datos que se muestran en la main section
 * 
 * @method changeMainData
 * @param {String} pokemonName 
 * @author Pedro Nel Builes Moreno
 * @returns void
 */
const changeMainData = (pokemonName) => {
    const pokeData = POKEMONS.filter(pokemon => pokemon.name === pokemonName)[0];
    console.log(pokeData);
    document.querySelector(".title__pokemon h2").innerHTML = pokeData.name;
    document.querySelector(".icon__type__pokemon img").src = `${ROUTEIMAGES}${pokeData.typeImage}`;
    document.querySelector(".image__pokemon img").src = `${ROUTEIMAGES}${pokeData.image}`;
    document.getElementById("valueNumber").innerHTML = pokeData.number;
    document.getElementById("valueLevel").innerHTML = pokeData.level;
    document.getElementById("valueType").innerHTML = pokeData.type;
    document.getElementById("valueHability").innerHTML = pokeData.hability;
    document.getElementById("valueHeight").innerHTML = pokeData.height;
    document.getElementById("valueWeight").innerHTML = pokeData.weight;

    updateOthersPokemons(pokemonName);
}

/**
 * Función encargada de cambiar los datos que se muestran en los pokemons restantes del footer section
 * 
 * @method updateOthersPokemons
 * @param {String} pokemonName 
 * @author Pedro Nel Builes Moreno
 * @returns void
 */
const updateOthersPokemons = (pokemonName) => {
    const othersPokemonsData = POKEMONS.filter(pokemon => pokemon.name !== pokemonName);
    for (let index = 0; index < othersPokemonsData.length; index++) {
        document.querySelectorAll(`.grid__others__pokemon .card__pokemon`)[index].setAttribute('onclick', `changeMainData("${othersPokemonsData[index].name}")`);
        document.querySelectorAll(`.grid__others__pokemon .card__pokemon img`)[index].src = `${ROUTEIMAGES}${othersPokemonsData[index].image}`;
    }
}